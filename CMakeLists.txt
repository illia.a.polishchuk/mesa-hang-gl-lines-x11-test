cmake_minimum_required(VERSION 3.12)

project(mesa-hang-gl-lines-x11-test)

if(MSVC)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /Zi")
else()
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g")
endif()

find_package(OpenGL REQUIRED)

set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)

add_subdirectory(${CMAKE_SOURCE_DIR}/glfw-3.3.8)

add_executable(${CMAKE_PROJECT_NAME} ${PROJECT_NAME}.cpp)
target_link_libraries(${CMAKE_PROJECT_NAME} PRIVATE OpenGL::GL glfw ${CMAKE_DL_LIBS})
target_compile_features(${CMAKE_PROJECT_NAME} PUBLIC cxx_std_11)

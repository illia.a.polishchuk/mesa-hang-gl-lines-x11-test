https://gitlab.freedesktop.org/mesa/mesa/-/issues/7892
## Building
```bash
mkdir build
cd build
cmake ..
cmake --build .
```
### The hang can be reproduced only if the all following conditions are met:
- X11 window with not zero samples like **glfwWindowHint(GLFW_SAMPLES, 2)**;<br /> 
- **glDisable(GL_MULTISAMPLE);**<br /> 
- **glLineWidth(1.5);**<br />
- **glEnable(GL_LINE_SMOOTH);**<br />
- **glDrawArrays(GL_LINES, ...);**<br /> (Note: With GL_TRIANGLES it works fine)

**Note:** works fine with other gpus and SOFTWARE drivers
